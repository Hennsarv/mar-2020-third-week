﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities db = new NorthwindEntities();
            db.Database.Log = Console.WriteLine;
            //foreach (var product in db.Products.ToList()
            //    .Select(x => new {x.ProductName, x.UnitPrice, CategoryName = x.Category?.CategoryName??"kategooriat pole"})
            //    ) 
            //    Console.WriteLine(product)
            //    ;

            //foreach (var c in db.Categories.Include("Products")
            //    .Select(x => new {x.CategoryName, x.Products.Count, Kalleim = x.Products.Max(y => y.UnitPrice)})
            //    )
            //    Console.WriteLine(c)
            //    ;


            //foreach (var e in db.Employees.ToList()
            //    .Select(e => new { e.FullName, ManagerName = e.Manager?.FullName??"ise jumal"})
            //    ) Console.WriteLine(e);

            var qProducts = db.Products
                .Where(x => !x.Discontinued)
                //.ToList()
                ;

            var q2 = qProducts.Select(x => new { x.ProductName, x.UnitPrice });

            var q3 = from p in db.Products
                     where !p.Discontinued
                     select new { p.ProductName, p.UnitPrice };

            //foreach (var p in q2) Console.WriteLine(p);

            Console.WriteLine(
                db.Products
                .Where(x => x.ProductName == "Lakkalikööri")
                .FirstOrDefault()
                .UnitPrice
            );



            Console.WriteLine(db.Products.Find(76).ProductName);
            //db.Products.Find(76).UnitPrice += 2;


            //db.Categories.Find(8).CategoryName = "Seafood";

            var tl = db.Products.Select(x =>   x.ProductName ).ToList();


            db.SaveChanges();

            Console.WriteLine(string.Join("\n",db.Categories.Select(x => x.CategoryName)));

            // kas on muudatusi
            if (db.ChangeTracker.HasChanges()) db.SaveChanges();

        }
    }

    partial class Employee
    {
        public string FullName => $"{FirstName} {LastName}";
    }
}
