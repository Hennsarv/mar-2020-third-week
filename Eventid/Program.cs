﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eventid
{
    class Program
    {
        static void Pidu(Inimene x) => Console.WriteLine($"täna saab pidu {x.Nimi} saab {x.Vanus} aastaseks");

        static void Main(string[] args)
        {
            Action<Inimene> p = (Inimene x) =>
            {
                Console.WriteLine($"tuleb minna {x.Nimi}u jaoks kinki ostma");
                Console.WriteLine("klapime raha");
            };


            Inimene henn = new Inimene { Nimi = "Henn", Vanus = 65 };
            henn.Juubel += Pidu;
            henn.Juubel += p;
            Inimene ants = new Inimene { Nimi = "Ants", Vanus = 60 };
            ants.Juubel += Pidu;
            ants.Pension += x => Console.WriteLine($"{x.Nimi} mine pinsile");
            ants.Pension += p;

            for (int i = 0; i < 35; i++)
            {
                henn.Vanus++;
                ants.Vanus++;
            }

            Console.WriteLine($"{henn.Nimi} on nüüd {henn.Vanus} aastane");
        }
    }

    class Inimene
    {

        public event Action<Inimene> Juubel;
        public event Action<Inimene> Pension;
        public string Nimi { get; set; }
        int _Vanus;
        public int Vanus { 
            get => _Vanus; 
            set
            {
                _Vanus = value;
                if(_Vanus % 25 == 0) if (Juubel != null) Juubel(this);
                

                if (_Vanus > 65) Pension?.Invoke(this);
            }
        }


    }

}
