﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace Õpsid
{
    public class IsePuhastav : IDisposable
    {

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~IsePuhastav()
        {
           // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
           Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion

    }


    public static class E
    {
        public static IEnumerable<string> ReadLineFromFile(this string filename)
        {
            using(TextReader tr = new StreamReader(filename))
            {
                for (string r = tr.ReadLine(); r != null; r = tr.ReadLine())
                    yield return r;
                    
            }
        }
    }

    public class Inimene
    {
        public string IK { get; set; }
        public string Nimi { get; set; }
        public string Aine { get; set; }
        public string Klass { get; set; }

        public string Kodulinn { get; set; }
        public DateTime Synniaeg { get; set; }
    }

    class Program
    {
        public void Main()
        {
            
           



            

            //MainLugemine(null);
            //MainKirjuta();
            MainKolmas();
        }

        static void MainKolmas()
        {
            using (TextReader tr = new StreamReader("..\\..\\õpetajad.txt"))
            {
                while (true)
                {
                    string r = tr.ReadLine();
                    if (r == null) break;
                    Console.WriteLine(r);
                }
            }

            using (TextWriter tw = new StreamWriter("..\\..\\test.txt"))
            {
                int[] arvud = { 1, 2, 3, 4, 5 };
                foreach (var a in arvud) tw.WriteLine(a);
            }

            foreach (var x in "..\\..\\test.txt".ReadLineFromFile()) Console.WriteLine(x);
        }



        static void MainKirjuta()
        {
            string loe = System.IO.File.ReadAllText("..\\..\\Õpetajad.json");

            var õpetajad = JsonConvert.DeserializeObject<List<Inimene>>(loe);

            foreach (var x in õpetajad) Console.WriteLine(x.Nimi + " " + x.Synniaeg);


        }

        static void MainLugemine(string[] args)
        {
            var õpsid = System.IO.File.ReadAllLines("..\\..\\Õpetajad.txt");


            var õpsid2 = õpsid
                .Where(x => x.Trim().Length > 0)
                .Select(x => (x + ",").Split(','))
                .Select(x => new Inimene { IK = x[0].Trim(), Nimi = x[1].Trim(), Aine = x[2].Trim(), Klass = x[3].Trim() })
                //.ToArray()
                ;
            //Console.WriteLine(õpsid[2]);
            //Console.WriteLine("asi on ilus");

            System.IO.File.WriteAllLines("..\\..\\Uued Õpetajad.txt",
                õpsid2
                .Select(x => $"{x.IK}\t{x.Nimi}\t{x.Aine}{(x.Klass == "" ? "" : "\t" + x.Klass)}")

            );
            // siit alates hakkab täiendus
            string json = JsonConvert.SerializeObject(õpsid2);
            System.IO.File.WriteAllText("..\\..\\Õpetajad.json", json);


        }
    }
}
