﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EFAken
{
    

    public partial class ProductsAken : MyAken
    {
        int categoryId = 0;
        string catName = "";
        public ProductsAken()
        {
            InitializeComponent();

            this.comboBox1.DataSource =
                   (new string[] { "Kõik tooted" })
                   .Union(
                        db.Categories
                        .Select(x => x.CategoryName)
                        //.ToList()
                   )
                   .ToList()
                   ;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
            {
                dataGridView1.DataSource = db.Products
                    .Where(x => categoryId == 0 || x.CategoryID == categoryId)
                    .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice, x.UnitsInStock, x.Category.CategoryName})
                    .ToList();

                dataGridView1.Columns["UnitPrice"].DefaultCellStyle.Format = "F2";
                dataGridView1.Columns["UnitPrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dataGridView1.Columns["UnitsInStock"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }

            this.Text = categoryId == 0 ? "Kõik tooted" : $"Kategooria {catName} tooted";

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            catName = comboBox1.SelectedItem.ToString();
            
            {
                categoryId = db.Categories.Where(x => x.CategoryName == catName).SingleOrDefault()?.CategoryID??0;
            }
            Form1_Load(sender, e);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CustomersAken f2 = new CustomersAken();
            f2.Show();
        }
    }
}
