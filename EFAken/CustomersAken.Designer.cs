﻿namespace EFAken
{
    partial class CustomersAken
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.countriesBox = new System.Windows.Forms.ComboBox();
            this.citiesBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 79);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(749, 359);
            this.dataGridView1.TabIndex = 0;
            // 
            // countriesBox
            // 
            this.countriesBox.FormattingEnabled = true;
            this.countriesBox.Location = new System.Drawing.Point(13, 36);
            this.countriesBox.Name = "countriesBox";
            this.countriesBox.Size = new System.Drawing.Size(121, 21);
            this.countriesBox.TabIndex = 1;
            this.countriesBox.SelectedIndexChanged += new System.EventHandler(this.countriesBox_SelectedIndexChanged);
            // 
            // citiesBox
            // 
            this.citiesBox.FormattingEnabled = true;
            this.citiesBox.Location = new System.Drawing.Point(172, 35);
            this.citiesBox.Name = "citiesBox";
            this.citiesBox.Size = new System.Drawing.Size(121, 21);
            this.citiesBox.TabIndex = 2;
            this.citiesBox.SelectedIndexChanged += new System.EventHandler(this.citiesBox_SelectedIndexChanged);
            // 
            // CustomersAken
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.citiesBox);
            this.Controls.Add(this.countriesBox);
            this.Controls.Add(this.dataGridView1);
            this.Name = "CustomersAken";
            this.Text = "Meie kliendid";
            this.Load += new System.EventHandler(this.CustomersAken_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox countriesBox;
        private System.Windows.Forms.ComboBox citiesBox;
    }
}