﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EFAken
{
    public partial class CustomersAken : MyAken
    {
        string selectedCountry = "";
        string selectedCity = "";

        public CustomersAken()
        {
            InitializeComponent();

            this.countriesBox.DataSource =
                (new string[] {"Kõik maad"})
                .Union(
                    db.Customers
                    .Select(x => x.Country)
                    .Distinct()
                    .OrderBy(x => x)
                )
                //.OrderBy(x => x)
                .ToList();
        }

        private void CustomersAken_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource =
                db.Customers
                .Where(c => selectedCountry == "Kõik maad" || c.Country == selectedCountry)
                .Where(c => selectedCity == "Kõik linnad" || c.City == selectedCity)
                .ToList();

            this.Text = selectedCountry + " kliendid";

        }

        private void countriesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedCountry = countriesBox.SelectedItem.ToString();

            citiesBox.DataSource =
                (new string[] { "Kõik linnad" })
                .Union(
                    db.Customers
                    .Where(c => selectedCountry == "Kõik maad" || c.Country == selectedCountry)
                    .Select(x => x.City)
                    .Distinct()
                    )
                .ToList();

            CustomersAken_Load(sender, e);
        }

        private void citiesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedCity = citiesBox.SelectedItem.ToString();

            CustomersAken_Load(sender, e);
        }
    }
}
