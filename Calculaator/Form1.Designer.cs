﻿namespace Calculaator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nupp0 = new System.Windows.Forms.Button();
            this.nupp1 = new System.Windows.Forms.Button();
            this.nupp4 = new System.Windows.Forms.Button();
            this.nupp7 = new System.Windows.Forms.Button();
            this.nupp2 = new System.Windows.Forms.Button();
            this.nupp3 = new System.Windows.Forms.Button();
            this.nupp5 = new System.Windows.Forms.Button();
            this.nupp6 = new System.Windows.Forms.Button();
            this.nupp8 = new System.Windows.Forms.Button();
            this.nupp9 = new System.Windows.Forms.Button();
            this.arvutamisNupp = new System.Windows.Forms.Button();
            this.kustutamisNupp = new System.Windows.Forms.Button();
            this.liitmisNupp = new System.Windows.Forms.Button();
            this.lahutamisNupp = new System.Windows.Forms.Button();
            this.korrutamisNupp = new System.Windows.Forms.Button();
            this.jagamisNupp = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.komaNupp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // nupp0
            // 
            this.nupp0.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp0.Location = new System.Drawing.Point(105, 233);
            this.nupp0.Name = "nupp0";
            this.nupp0.Size = new System.Drawing.Size(49, 44);
            this.nupp0.TabIndex = 0;
            this.nupp0.Text = "0";
            this.nupp0.UseVisualStyleBackColor = true;
            this.nupp0.Click += new System.EventHandler(this.Number_Click);
            // 
            // nupp1
            // 
            this.nupp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp1.Location = new System.Drawing.Point(105, 183);
            this.nupp1.Name = "nupp1";
            this.nupp1.Size = new System.Drawing.Size(49, 44);
            this.nupp1.TabIndex = 1;
            this.nupp1.Text = "1";
            this.nupp1.UseVisualStyleBackColor = true;
            this.nupp1.Click += new System.EventHandler(this.Number_Click);
            // 
            // nupp4
            // 
            this.nupp4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp4.Location = new System.Drawing.Point(105, 133);
            this.nupp4.Name = "nupp4";
            this.nupp4.Size = new System.Drawing.Size(49, 44);
            this.nupp4.TabIndex = 2;
            this.nupp4.Text = "4";
            this.nupp4.UseVisualStyleBackColor = true;
            this.nupp4.Click += new System.EventHandler(this.Number_Click);
            // 
            // nupp7
            // 
            this.nupp7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp7.Location = new System.Drawing.Point(105, 83);
            this.nupp7.Name = "nupp7";
            this.nupp7.Size = new System.Drawing.Size(49, 44);
            this.nupp7.TabIndex = 3;
            this.nupp7.Text = "7";
            this.nupp7.UseVisualStyleBackColor = true;
            this.nupp7.Click += new System.EventHandler(this.Number_Click);
            // 
            // nupp2
            // 
            this.nupp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp2.Location = new System.Drawing.Point(160, 183);
            this.nupp2.Name = "nupp2";
            this.nupp2.Size = new System.Drawing.Size(49, 44);
            this.nupp2.TabIndex = 4;
            this.nupp2.Text = "2";
            this.nupp2.UseVisualStyleBackColor = true;
            this.nupp2.Click += new System.EventHandler(this.Number_Click);
            // 
            // nupp3
            // 
            this.nupp3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp3.Location = new System.Drawing.Point(215, 183);
            this.nupp3.Name = "nupp3";
            this.nupp3.Size = new System.Drawing.Size(49, 44);
            this.nupp3.TabIndex = 5;
            this.nupp3.Text = "3";
            this.nupp3.UseVisualStyleBackColor = true;
            this.nupp3.Click += new System.EventHandler(this.Number_Click);
            // 
            // nupp5
            // 
            this.nupp5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp5.Location = new System.Drawing.Point(160, 133);
            this.nupp5.Name = "nupp5";
            this.nupp5.Size = new System.Drawing.Size(49, 44);
            this.nupp5.TabIndex = 6;
            this.nupp5.Text = "5";
            this.nupp5.UseVisualStyleBackColor = true;
            this.nupp5.Click += new System.EventHandler(this.Number_Click);
            // 
            // nupp6
            // 
            this.nupp6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp6.Location = new System.Drawing.Point(215, 133);
            this.nupp6.Name = "nupp6";
            this.nupp6.Size = new System.Drawing.Size(49, 44);
            this.nupp6.TabIndex = 7;
            this.nupp6.Text = "6";
            this.nupp6.UseVisualStyleBackColor = true;
            this.nupp6.Click += new System.EventHandler(this.Number_Click);
            // 
            // nupp8
            // 
            this.nupp8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp8.Location = new System.Drawing.Point(160, 83);
            this.nupp8.Name = "nupp8";
            this.nupp8.Size = new System.Drawing.Size(49, 44);
            this.nupp8.TabIndex = 8;
            this.nupp8.Text = "8";
            this.nupp8.UseVisualStyleBackColor = true;
            this.nupp8.Click += new System.EventHandler(this.Number_Click);
            // 
            // nupp9
            // 
            this.nupp9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.nupp9.Location = new System.Drawing.Point(215, 83);
            this.nupp9.Name = "nupp9";
            this.nupp9.Size = new System.Drawing.Size(49, 44);
            this.nupp9.TabIndex = 9;
            this.nupp9.Text = "9";
            this.nupp9.UseVisualStyleBackColor = true;
            this.nupp9.Click += new System.EventHandler(this.Number_Click);
            // 
            // arvutamisNupp
            // 
            this.arvutamisNupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.arvutamisNupp.Location = new System.Drawing.Point(160, 233);
            this.arvutamisNupp.Name = "arvutamisNupp";
            this.arvutamisNupp.Size = new System.Drawing.Size(49, 44);
            this.arvutamisNupp.TabIndex = 10;
            this.arvutamisNupp.Text = "=";
            this.arvutamisNupp.UseVisualStyleBackColor = true;
            this.arvutamisNupp.Click += new System.EventHandler(this.Võrdus_Click);
            // 
            // kustutamisNupp
            // 
            this.kustutamisNupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.kustutamisNupp.Location = new System.Drawing.Point(215, 233);
            this.kustutamisNupp.Name = "kustutamisNupp";
            this.kustutamisNupp.Size = new System.Drawing.Size(49, 44);
            this.kustutamisNupp.TabIndex = 11;
            this.kustutamisNupp.Text = "C";
            this.kustutamisNupp.UseVisualStyleBackColor = true;
            this.kustutamisNupp.Click += new System.EventHandler(this.C_Click);
            // 
            // liitmisNupp
            // 
            this.liitmisNupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.liitmisNupp.Location = new System.Drawing.Point(270, 83);
            this.liitmisNupp.Name = "liitmisNupp";
            this.liitmisNupp.Size = new System.Drawing.Size(49, 44);
            this.liitmisNupp.TabIndex = 12;
            this.liitmisNupp.Text = "+";
            this.liitmisNupp.UseVisualStyleBackColor = true;
            this.liitmisNupp.Click += new System.EventHandler(this.Tehe_Click);
            // 
            // lahutamisNupp
            // 
            this.lahutamisNupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lahutamisNupp.Location = new System.Drawing.Point(270, 133);
            this.lahutamisNupp.Name = "lahutamisNupp";
            this.lahutamisNupp.Size = new System.Drawing.Size(49, 44);
            this.lahutamisNupp.TabIndex = 13;
            this.lahutamisNupp.Text = "-";
            this.lahutamisNupp.UseVisualStyleBackColor = true;
            this.lahutamisNupp.Click += new System.EventHandler(this.Tehe_Click);
            // 
            // korrutamisNupp
            // 
            this.korrutamisNupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.korrutamisNupp.Location = new System.Drawing.Point(270, 183);
            this.korrutamisNupp.Name = "korrutamisNupp";
            this.korrutamisNupp.Size = new System.Drawing.Size(49, 44);
            this.korrutamisNupp.TabIndex = 14;
            this.korrutamisNupp.Text = "*";
            this.korrutamisNupp.UseVisualStyleBackColor = true;
            this.korrutamisNupp.Click += new System.EventHandler(this.Tehe_Click);
            // 
            // jagamisNupp
            // 
            this.jagamisNupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.jagamisNupp.Location = new System.Drawing.Point(270, 233);
            this.jagamisNupp.Name = "jagamisNupp";
            this.jagamisNupp.Size = new System.Drawing.Size(49, 44);
            this.jagamisNupp.TabIndex = 15;
            this.jagamisNupp.Text = "/";
            this.jagamisNupp.UseVisualStyleBackColor = true;
            this.jagamisNupp.Click += new System.EventHandler(this.Tehe_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox1.Location = new System.Drawing.Point(105, 284);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(214, 38);
            this.textBox1.TabIndex = 16;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.textBox2.Location = new System.Drawing.Point(105, 328);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(214, 38);
            this.textBox2.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(105, 373);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 31);
            this.label1.TabIndex = 18;
            // 
            // komaNupp
            // 
            this.komaNupp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.komaNupp.Location = new System.Drawing.Point(325, 233);
            this.komaNupp.Name = "komaNupp";
            this.komaNupp.Size = new System.Drawing.Size(49, 44);
            this.komaNupp.TabIndex = 19;
            this.komaNupp.Text = ",";
            this.komaNupp.UseVisualStyleBackColor = true;
            this.komaNupp.Click += new System.EventHandler(this.Koma_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 450);
            this.Controls.Add(this.komaNupp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.jagamisNupp);
            this.Controls.Add(this.korrutamisNupp);
            this.Controls.Add(this.lahutamisNupp);
            this.Controls.Add(this.liitmisNupp);
            this.Controls.Add(this.kustutamisNupp);
            this.Controls.Add(this.arvutamisNupp);
            this.Controls.Add(this.nupp9);
            this.Controls.Add(this.nupp8);
            this.Controls.Add(this.nupp6);
            this.Controls.Add(this.nupp5);
            this.Controls.Add(this.nupp3);
            this.Controls.Add(this.nupp7);
            this.Controls.Add(this.nupp4);
            this.Controls.Add(this.nupp2);
            this.Controls.Add(this.nupp1);
            this.Controls.Add(this.nupp0);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button nupp0;
        private System.Windows.Forms.Button nupp1;
        private System.Windows.Forms.Button nupp4;
        private System.Windows.Forms.Button nupp7;
        private System.Windows.Forms.Button nupp2;
        private System.Windows.Forms.Button nupp3;
        private System.Windows.Forms.Button nupp5;
        private System.Windows.Forms.Button nupp6;
        private System.Windows.Forms.Button nupp8;
        private System.Windows.Forms.Button nupp9;
        private System.Windows.Forms.Button arvutamisNupp;
        private System.Windows.Forms.Button kustutamisNupp;
        private System.Windows.Forms.Button liitmisNupp;
        private System.Windows.Forms.Button lahutamisNupp;
        private System.Windows.Forms.Button korrutamisNupp;
        private System.Windows.Forms.Button jagamisNupp;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button komaNupp;
    }
}

