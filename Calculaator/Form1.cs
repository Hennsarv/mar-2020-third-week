﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculaator
{
    public partial class Form1 : Form
    {
        int kumb = 0;
        string tehe = "";
        decimal? tulemus = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void Number_Click(object sender, EventArgs e)
        {

            (kumb == 0 ? textBox1 : textBox2).Text += ((Button)sender).Text;
        }

        private void Tehe_Click(object sender, EventArgs e)
        {

            if (tulemus.HasValue)
            {
                textBox1.Text = tulemus.ToString();
                textBox2.Text = "";
                tulemus = null;
                label1.Text = "";
            }

            if (textBox1.Text == "") return;
            tehe = ((Button)sender).Text;
            kumb = 1;
            liitmisNupp.BackColor = lahutamisNupp.BackColor = korrutamisNupp.BackColor = jagamisNupp.BackColor = this.BackColor;
            ((Button)sender).BackColor = Color.White;
        }

        private void Võrdus_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || tehe == "") return;
            try
            {
                decimal x = decimal.Parse(textBox1.Text); //, System.Globalization.CultureInfo.CurrentCulture);
                decimal y = decimal.Parse(textBox2.Text); //, System.Globalization.CultureInfo.CurrentCulture);
                tulemus =
                    tehe == "+" ? x + y :
                    tehe == "-" ? x - y :
                    tehe == "*" ? x * y :
                    tehe == "/" ? x / y : 0;
                label1.Text = tulemus.ToString();

            }
            catch (Exception ex)
            {
                label1.Text = ex.Message;
               
            }
        }

        private void C_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox2.Text = label1.Text = tehe = "";
            kumb = 0;
            liitmisNupp.BackColor = lahutamisNupp.BackColor = korrutamisNupp.BackColor = jagamisNupp.BackColor = this.BackColor;
            tulemus = null;
        }

        private void Koma_Click(object sender, EventArgs e)
        {
            // TODO: läbi tuleks mõelda, kuidas see rakendus USAsse müüa (. mitte ,)
            var nupp = kumb == 0 ? textBox1 : textBox2;
            if (nupp.Text.Contains(",")) return;
            nupp.Text += ",";
        }
    }
}
