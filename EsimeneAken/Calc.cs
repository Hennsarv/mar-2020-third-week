﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsimeneAken
{
    public partial class Calc : Form
    {
        int kumb = 0;
        string tehe = "";

        public Calc()
        {
            InitializeComponent();
        }

        private void Number_Click(object sender, EventArgs e)
        {
            (kumb == 0 ? textBox1 : textBox2).Text += ((Button)sender).Text;
        }

        private void Tehe_Click(object sender, EventArgs e)
        {
            tehe = ((Button)sender).Text;
            kumb = 1;
            button12.BackColor = button13.BackColor = button14.BackColor = button15.BackColor = button16.BackColor;
            ((Button)sender).BackColor = Color.White;
        }

        private void Võrdus_Click(object sender, EventArgs e)
        {
            try
            {
                int x = int.Parse(textBox1.Text);
                int y = int.Parse(textBox2.Text);
                int tulemus =
                tehe == "+" ? x + y :
                tehe == "-" ? x - y :
                tehe == "*" ? x * y :
                tehe == "/" ? x / y : 0;
                label1.Text = tulemus.ToString();
            }
            catch (Exception ex)
            {
                label1.Text = ex.Message;
                
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox2.Text = label1.Text = "";
            kumb = 0;
            tehe = "";
            button12.BackColor = button13.BackColor = button14.BackColor = button15.BackColor = button16.BackColor;
        }
    }
}
