﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsimeneAken
{
    public partial class Form1 : Form
    {
        bool kasolirahul = false;
        public Form1()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.label2.Text = "Ära siis virise";
            kasolirahul = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.label2.Text = "Paneme siis sulle palka jurde";
        }

        private void button2_MouseHover(object sender, EventArgs e)
        {
            this.button2.Visible = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            e.Cancel = !kasolirahul;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(sender is Button nupp)
            {
                label2.Text = "vajutati nuppu: " + nupp.Text;
            }

            label2.Text = "vajutati nuppu: " + ((Button)sender).Text;
        }
    }
}
